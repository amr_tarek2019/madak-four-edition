<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlaceBooking extends Model
{
    protected $table='places_booking';
    protected $fillable=['user_id', 'place_id', 'date_from', 'date_to', 'month', 'year','status'];

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function place()
    {
        return $this->belongsTo('App\User','place_id');
    }
}
