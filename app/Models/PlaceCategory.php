<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlaceCategory extends Model
{
    protected $table='places_categories';
    protected $fillable=[ 'image', 'name','status'];


}
