<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceSubSubCategory extends Model
{
    protected $table='services_sub_sub_categories';
    protected $fillable=['service_subcategory_id', 'image', 'name','status'];

    public function serviceSubCategory()
    {
        return $this->belongsTo('App\ServiceSubCategory','service_subcategory_id');
    }
}
