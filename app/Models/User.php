<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class User extends Authenticatable
{
    use Notifiable,HelperTrait;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','phone','lat','lng',
        'firebase_token','jwt_token','image','verify_code','user_type','notification_status',
        'user_status','status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
       'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function setImageAttribute($file)
    {
        if (is_file($file)) {
            $fileName = $this->createFileName($file);
            $this->originalImage($file, $fileName,'user/original');
            $this->mediumImage($file, $fileName,150,150,'user/meduim');
            $this->thumbImage($file, $fileName, 70,70,'user/thumbnail');

            $this->attributes['image'] = $fileName;
        }

    }
    public function getImageAttribute()
    {
        if($this->attributes['image']!=null) {
            return SiteImages_path('user') . '/original/' . $this->attributes['image'];
        }
        else{

            return SiteImages_path('user') . '/defaultuser.png';

        }
    }
}
