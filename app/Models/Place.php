<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Place extends Model
{
    use HelperTrait ;
    protected $table='places';
    protected $fillable=['provider_id', 'place_category_id',
        'name', 'present_type', 'price', 'quantity', 'date_from',
        'date_to','month','year', 'lat', 'lng', 'address', 'desc','main_image'];

    public function provider()
    {
        return $this->belongsTo('App\Provider','provider_id');
    }

    public function placeCategory()
    {
        return $this->belongsTo('App\PlaceCategory','place_category_id');
    }


    public function related_images(){

        return $this->hasMany(PlaceImage::class, 'place_id', 'id')->select('image','place_id');
    }

    public function related_provider(){
        return $this->belongsTo(Providers::class, 'provider_id','provider_id');
    }

    public function related_category(){
        return $this->hasMany('App\Models\PlaceCategory', 'place_category_id','id');
    }

    public function GetImage($place){
        return $place->related_default_image;
    }

    public function getMainImageAttribute(){
        if($this->attributes['main_image']!=null) {
            return SiteImages_path('places/main_image') . '/original/' . $this->attributes['main_image'];
        }
        else{

            return SiteImages_path('places/main_image') . '/default.png';

        }


    }
    public function setMainImageAttribute($file)
    {
        if ($file) {
            $fileName = $this->createFileName($file);
            $this->originalImage($file, $fileName,'places/main_image/original');
            $this->mediumImage($file, $fileName,150,150,'places/main_image/meduim');
            $this->thumbImage($file, $fileName, 70,70,'places/main_image/thumbnail');

            $this->attributes['main_image'] = $fileName;
        }

    }

    public function getImageAttribute(){
        if($this->attributes['image']!=null) {
            return SiteImages_path('places') . '/original/' . $this->attributes['image'];
        }
        else{

            return SiteImages_path('places') . '/default.png';

        }


    }
    public function setImageAttribute($file)
    {
        if ($file) {
            $fileName = $this->createFileName($file);
            $this->originalImage($file, $fileName,'places/original');
            $this->mediumImage($file, $fileName,150,150,'places/meduim');
            $this->thumbImage($file, $fileName, 70,70,'places/thumbnail');
            $this->attributes['image'] = $fileName;
        }

    }

}
