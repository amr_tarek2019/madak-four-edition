<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceCategory extends Model
{
    protected $table='services_categories';
    protected $fillable=['name', 'image','status'];

    public function department()
    {
        return $this->belongsTo('App\Department','department_id');
    }
}
