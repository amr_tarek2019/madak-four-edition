<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceProviderRequest extends Model
{
    protected $table='service_provider_request';
    protected $fillable=['user_id','status'];
    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
}
