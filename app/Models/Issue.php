<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Issue extends Model
{
    protected $table='issues';
    protected $fillable=['text','status'];

    public function getAllIssues()
    {
        return static::all();
    }
}
