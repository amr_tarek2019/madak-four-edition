<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PreparationBooking extends Model
{
    protected $table='preparation_booking';
    protected $fillable=['user_id','message', 'date_from', 'date_to', 'total', 'month', 'year','status'];

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function service()
    {
        return $this->belongsTo('App\Service','service_id');
    }
}
