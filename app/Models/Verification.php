<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;
class Verification extends Model
{
    protected $table="verifications";

    protected $primaryKey='verifications_id';


    protected $fillable=['user_id', 'verifications_code'];



    public function related_user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'user_id');

    }

}
