<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Providers extends Model
{
    protected $table='providers';
    protected $fillable=['user_id'];

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function related_places(){
        return $this->hasMany('App\Models\Place', 'provider_id')
            ->with('related_images');
    }
    public function related_services(){
        return $this->hasMany('App\Models\Service', 'provider_id')
            ->with('related_images');
    }

    public function related_images(){

        return $this->hasMany(PlaceImage::class, 'place_id', 'id')->select('image','place_id');
    }

    public function getImageAttribute(){
        if($this->attributes['image']!=null) {
            return SiteImages_path('places') . '/original/' . $this->attributes['image'];
        }
        else{

            return SiteImages_path('places') . '/default.png';

        }


    }
    public function setImageAttribute($file)
    {
        if ($file) {
            $fileName = $this->createFileName($file);
            $this->originalImage($file, $fileName,'places/original');
            $this->mediumImage($file, $fileName,150,150,'places/meduim');
            $this->thumbImage($file, $fileName, 70,70,'places/thumbnail');
            $this->attributes['image'] = $fileName;
        }

    }
}
