<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Contact extends Model
{
    protected $table='contacts';
    protected $fillable=['user_id', 'issue_id', 'name', 'email', 'message'];

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function issue()
    {
        return $this->belongsTo('App\Issue','issue_id');
    }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->toDateString();
    }

//    public function create(array $attributes){
//        return $this->contact->create($attributes);
//    }

    public function related_user(){

        return $this->belongsTo(User::class, 'user_id', 'user_id');
    }

}
