<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $table='notifications';
    protected $fillable=['user_id', 'icon', 'title', 'text'];

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
    public function setIconAttribute($file)
    {
        if (is_file($file)) {
            $fileName = $this->createFileName($file);
            $this->originalImage($file, $fileName,'notify/original');
            $this->mediumImage($file, $fileName,150,150,'notify/meduim');
            $this->thumbImage($file, $fileName, 70,70,'notify/thumbnail');

            $this->attributes['icon'] = $fileName;
        }
        else{
            $this->attributes['icon']=$file;
        }

    }


    public function getIconAttribute(){

        if(!filter_var($this->attributes['icon'], FILTER_VALIDATE_URL)) {


            return SiteImages_path('notify') . '/original/' . $this->attributes['icon'];
        }

        elseif(filter_var($this->attributes['icon'], FILTER_VALIDATE_URL)){

            return $this->attributes['icon'];

        }
        else{
            return SiteImages_path('notify') . '/default.png';

        }


    }
    public function getCreatedAtAttribute(){
        return Carbon::createFromTimeStamp(strtotime($this->attributes['created_at']))->diffForHumans();



    }
}
