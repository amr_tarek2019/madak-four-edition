<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your module. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::get('/auth', function (Request $request) {
//    // return $request->auth();
//})->middleware('auth:api');
Route::group(['prefix' => 'Auth'], function () {
    Route::post('provider/login','Api\AuthController@LoginProvider');
    Route::get('/profile','Api\AuthController@UserProfile');
    Route::post('/updateprofile','Api\AuthController@UpdateProfile');
});
