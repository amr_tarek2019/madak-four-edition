<?php

namespace App\Modules\Auth\Http\Controllers\Api;

use App\Models\User;
use App\Modules\Auth\Http\Interfaces\AuthRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    protected $authObject;

    public function __construct(AuthRepositoryInterface $authRepository)
    {
        $this->authObject = $authRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function LoginProvider(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'email' => 'required',
                'password' => 'required',
                'firebase_token' => 'required',

            ]
        );

        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'msg' => $validator->messages()]);
        }



        $user = User::where('email',$request->email)->where('user_type','provider')->first();

        if (!$user) {
            return response()->json(res_msg($request->header('lang'), failed(), 404, 'user_not_found'));
        }

        $check = Hash::check($request->password, $user->password);
        if ($check) {

            if ($user->user_status == 0) {
                $this->authObject->Sendverificationcode($user);
                return response()->json(res_msg($request->header('lang'), failed(), 405, 'user_not_verified'));
            }
            $jwt = str_random(20);
            $user->update(array('jwt_token' => $jwt,'firebase_token'=>$request->firebase_token));


            // $user['country'] = $this->authObject->GetCountryNameById($user->FK_countries_id, $request->header('lang'));
            return response()->json(res_msg($request->header('lang'), success(), 200, 'logged_in', $user));
        } else {
            return response()->json(res_msg($request->header('lang'), failed(), 401, 'invalid_password'));
        }


    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function UserProfile()
    {
        $lang = getallheaders()['Lang'];

        $user = User::select('id','name','email','jwt_token','image','firebase_token'
            ,'phone','password')->whereJwtToken(getallheaders()['Jwt'])->first();
        return response()->json(res_msg($lang, success(), 200, 'user_profile', $user));


    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function UpdateProfile(Request $request)
    {
        $lang = $request->header('Lang');
        $data = $request->all();
        $user = User::select('id','name','email','jwt_token','image','firebase_token'
            ,'phone','password')->whereJwtToken(getallheaders()['Jwt'])->first();
        if ($user) {

            $validator = Validator::make($data,
                array(
                    'email' => 'unique:users,email,'.$user->id.',id',
                    'phone' => 'unique:users,phone,' . $user->id . ',id',
                )
            );


            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'msg' => $validator->messages()]);
            }

            $user->update($data);

            //$city = $this->authObject->GetCountryNameById($user->FK_countries_id, $request->header('Lang'));
            // $user['city'] = $city;

            return response()->json(res_msg($lang, success(), 200, 'updated', $user));
        } else {


            return response()->json(res_msg($lang, failed(), 404, 'user_not_found'));


        }


    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
