<?php

namespace App\Modules\Issue\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('issue', 'Resources/Lang', 'app'), 'issue');
        $this->loadViewsFrom(module_path('issue', 'Resources/Views', 'app'), 'issue');
        $this->loadMigrationsFrom(module_path('issue', 'Database/Migrations', 'app'), 'issue');
        $this->loadConfigsFrom(module_path('issue', 'Config', 'app'));
        $this->loadFactoriesFrom(module_path('issue', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
