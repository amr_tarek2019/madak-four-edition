<?php
namespace App\Modules\Issue\Http\Eloquent;

use App\Models\Issue;
use App\Modules\Issue\Http\Interfaces\IssueRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;

class IssueRepository implements IssueRepositoryInterface
{
    public function getIssues($lang)
    {

        $issues = Issue::where('status','1')->get();
        $allIssues = array();
        $i = 0;
        foreach ($issues as $issue) {

            $allIssues[$i] = array(
                'id'=>$issue->id,
                'name' => $issue->text);
            $i++;
        }
        return $allIssues;


    }

}
