<?php
namespace App\Modules\Issue\Http\Interfaces;

interface IssueRepositoryInterface
{
    public function getIssues($lang);
}



