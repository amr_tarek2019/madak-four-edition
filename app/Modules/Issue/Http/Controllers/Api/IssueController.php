<?php

namespace App\Modules\Issue\Http\Controllers\Api;

use App\Modules\Issue\Http\Interfaces\IssueRepositoryInterface;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class IssueController extends Controller
{
    protected $issueObject;


    public function __construct(IssueRepositoryInterface $issueRepository)
    {
        $this->issueObject = $issueRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIssues()
    {
        $lang=getallheaders()['Lang'];
        $result= $this->issueObject->getIssues($lang);
        return response(res_msg($lang, success(),200,'all_issues', $result));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
