<?php
namespace App\Modules\Service\Http\Eloquent;

use App\Models\Service;
use App\Modules\Service\Http\Interfaces\ServiceRepositoryInterface;

class ServiceRepository implements ServiceRepositoryInterface
{
    public function addService($request)
    {
        $service= Service::create([
            'service_category_id' => $request->service_category_id,
            'service_subcategory_id' => $request->service_subcategory_id,
            'service_sub_sub_category_id' => $request->service_sub_sub_category_id,
            'present_type' => $request->present_type,
            'price' => $request->price,
            'provider_id' => $request->provider_id,
            'quantity' => $request->quantity,
            'date_from' => $request->date_from,
            'date_to' => $request->date_to,
            'month' => $request->month,
            'year' => $request->year,
            'lat' => $request->lat,
            'lng' => $request->lng,
            'address' => $request->address,
            'desc' => $request->desc,
            'image' => $request->image,
        ]);

         return $service;
    }

}
