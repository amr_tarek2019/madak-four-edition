<?php
namespace App\Modules\Contact\Http\Eloquent;

use App\Models\Contact;
use App\Models\User;
use App\Modules\Contact\Http\Interfaces\ContactRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;

class ContactRepository implements ContactRepositoryInterface
{
    public function CheckJwt($jwt)
    {
        $user=User::where('jwt_token',$jwt)->first();
        if (isset($user->id)){
            return $user;
        }else{
            return "false";
        }
    }
    public function InsertContactForm($request)
    {
        $validator = Validator::make($request->all(),
            array(
                'email' => 'required',
                'name' => 'required',
                'issue_id' => 'required',
                'message'=>'required',
                'Jwt' => 'required'

            )
        );

        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'msg' => $validator->messages()]);
        }
        $user = $this->CheckJwt($request->Jwt);
        $request['user_id'] = $user->id;
        $request['issue_id']=$request->issue_id;
        Contact::create($request->all());
        return 'true';
    }

}
