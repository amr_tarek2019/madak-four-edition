<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your module. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::get('/provider', function (Request $request) {
//    // return $request->provider();
//})->middleware('auth:api');

Route::group(['prefix' => 'provider'], function () {
    Route::post('request','Api\ProviderController@serviceProviderRequest');
    Route::get('/allplacescategories','Api\ProviderController@getAllPlacesCategories');
    Route::get('/allservicescategories','Api\ProviderController@getAllServicesCategories');
    Route::get('/allservicessubcategories/{category_id}','Api\ProviderController@getAllServicesSubCategories');
    Route::get('/allservicessubsubcategories/{subcategory_id}','Api\ProviderController@getAllServicesSubSubCategories');
    Route::get('/allproviderplacescategoriesandservices/{provider_id}','Api\ProviderController@getAllProviderPlacesCategoriesAndServices');
    Route::get('/getallplacescategoriesitems/{place_category_id}','Api\ProviderController@getAllPlacesCategoriesItems');
    Route::get('/place/{place_id}','Api\ProviderController@getPlaceDetails');
    Route::Post('/updateplace','Api\ProviderController@editPlace');
});
