<?php
namespace App\Modules\Provider\Http\Eloquent;

use App\Models\Place;
use App\Models\PlaceCategory;
use App\Models\PlaceImage;
use App\Models\Providers;
use App\Models\Rate;
use App\Models\Service;
use App\Models\ServiceCategory;
use App\Models\ServiceProviderRequest;
use App\Models\ServiceSubcategory;
use App\Models\ServiceSubSubCategory;
use App\Models\User;
use App\Modules\Provider\Http\Interfaces\ServiceProviderRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use function foo\func;

class ProviderRepository implements ServiceProviderRepositoryInterface
{
    public function CheckJwt($jwt)
    {
        $user=User::where('jwt_token',$jwt)->first();
        if (isset($user->id)){
            return $user;
        }else{
            return "false";
        }
    }

    public function serviceProviderRequest($request)
{
    $validator = Validator::make($request->all(),
        array(
            'Jwt' => 'required'
        )
    );

    if ($validator->fails()) {
        return response()->json(['status' => 'error', 'msg' => $validator->messages()]);
    }
    $user = $this->CheckJwt($request->Jwt);
    $request['user_id'] = $user->id;

    ServiceProviderRequest::create($request->all());
    return 'true';
}

    public function getAllPlacesCategories($lang){
        $allcategories=array();
        $data=array();
        $categories = PlaceCategory::where('status','1')->get();
        foreach ($categories as $category) {
            $data['id']=$category->id;
            $data['name']=$category->name;
            array_push($allcategories,$data);
        }
        return $allcategories;
    }

    public function getAllServicesCategories($lang){
        $allcategories=array();
        $data=array();
        $categories = ServiceCategory::where('status','1')->get();
        foreach ($categories as $category) {
            $data['id']=$category->id;
            $data['name']=$category->name;
            array_push($allcategories,$data);
        }
        return $allcategories;
    }

    public function getAllServicesSubCategories($lang,$category_id){
        $allsubcategories=array();
        $data=array();
        $subcategories = ServiceSubcategory::where('service_category_id',$category_id)->where('status','1')->get();

        foreach ($subcategories as $subcategory) {
            $data['id']=$subcategory->id;
            $data['name']=$subcategory->name;
            array_push($allsubcategories,$data);
        }
        return $allsubcategories;
    }

    public function getAllServicesSubSubCategories($lang,$subcategory_id){
        $allsubsubcategories=array();
        $data=array();
        $subsubcategories = ServiceSubSubCategory::where('service_subcategory_id',$subcategory_id)->where('status','1')->get();

        foreach ($subsubcategories as $subsubcategory) {
            $data['id']=$subsubcategory->id;
            $data['name']=$subsubcategory->name;
            array_push($allsubsubcategories,$data);
        }
        return $allsubsubcategories;
    }

    public function getAllProviderPlacesCategories($lang){
        $allcategories=array();
        $data=array();
        $Jwt = getallheaders()['Jwt'];
        $user = $this->CheckJwt($Jwt);
        $provider_id=Providers::where('user_id',$user->id)->pluck('id')->first();
        $categories = PlaceCategory::where('status','1')->get();
        foreach ($categories as $category) {
            $data['id']=$category->id;
            $data['category_name']=$category->name;
            $data['main_image']=$category->main_image;
            $data['count']=Place::select('id')->where('provider_id',$provider_id)->where('place_category_id',$category->id)->count();
            array_push($allcategories,$data);
        }
        return $allcategories;
    }

    public function getAllProviderServices($lang){
        $allservices=array();
        $data=array();
        $Jwt = getallheaders()['Jwt'];
        $user = $this->CheckJwt($Jwt);
        $provider_id=Providers::where('user_id',$user->id)->pluck('id')->first();
        $services = Service::where('provider_id',$provider_id)->get();
        foreach ($services as $service) {
            $data['id']=$service->id;
            $data['service_name']=$service->name;
            $data['image']=$service->image;
            array_push($allservices,$data);
        }
        return $allservices;
    }
    public function getAllPlacesCategoriesItems($lang,$place_category_id)
    {
        $allitems = array();
        $data = array();
        $placesCategories = Place::where('place_category_id', $place_category_id)->orderBy('id', 'desc')->get();
        foreach ($placesCategories as $place) {
            $data['id']=$place->id;
            $data['image']=$place->main_image;
            $data['name']=$place->name;
            $data['desc']=$place->desc;
            $data['price']=$place->price;
            $ratePlace=Rate::where('place_id',$place->id)->select('rate')->avg('rate');
            if (!empty($ratePlace))
            {
            $data['rate']=(string)$ratePlace;
            }else{
                $data['rate']="not found ratings";
            }
            array_push($allitems,$data);
        }
        return $allitems;
    }

    public function getPlaceDetails($place_id,$lang){
        $place= Place::where('id',$place_id)->get();
        $res_item = [];
        $res_list  = [];
        foreach ($place as $res) {
            $res_item['id'] = $res->id;
            $res_item['name'] = $res->name;
            $category=PlaceCategory::where('id',$res->place_category_id)->select('id','name')->first();
            $res_item['category_id']=$category->id;
            $res_item['category_name']=$category->name;
            $images=PlaceImage::where('place_id',$res->id)->select('image')->get();
            $res_item['images']=$images;
            $res_item['present_type'] = $res->present_type;
            $res_item['present_type'] = $res->present_type;
            $res_item['quantity'] = $res->quantity;
            $res_item['price'] = $res->price;
            $res_item['date_from'] = $res->date_from;
            $res_item['date_to'] = $res->date_to;
            $res_item['month'] = $res->month;
            $res_item['year'] = $res->year;
            $res_item['lat'] = $res->lat;
            $res_item['lng'] = $res->lng;
            $res_item['address'] = $res->address;
            $res_item['desc'] = $res->desc;
            $res_list = $res_item;
        }
        return $res_list;
    }

    public function editPlace($request)
    {
        $place=Place::where('id',$request->place_id)->first();
        $place->update([
            'name'=>$request->name,
            'place_category_id'=>$request->place_category_id,
            'present_type'=>$request->present_type,
            'price'=>$request->price,
            'quantity'=>$request->quantity,
            'date_from'=>$request->date_from,
            'date_to'=>$request->date_to,
            'month'=>$request->month,
            'year'=>$request->year,
            'lat'=>$request->lat,
            'lng'=>$request->lng,
            'address'=>$request->address,
            'desc'=>$request->desc,
            'main_image'=>$request->main_image,
        ]);
        for ($i = 0; $i < count($request->imgs); $i++) {
            $placeImage=PlaceImage::where('place_id',$request->place_id)->first();
            $placeImage->related_images()->update(['image' => $request->imgs[$i],
                'image' =>  $request->imgs[$i],
            ]);
        }
    }
}
