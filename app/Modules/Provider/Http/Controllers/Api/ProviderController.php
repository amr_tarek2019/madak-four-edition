<?php

namespace App\Modules\Provider\Http\Controllers\Api;

use App\Models\Place;
use App\Models\Providers;
use App\Models\User;
use Illuminate\Http\Request;
use App\Modules\Provider\Http\Interfaces\ServiceProviderRepositoryInterface;
use App\Modules\Auth\Http\Interfaces\AuthRepositoryInterface;
use App\Http\Controllers\Controller;

class ProviderController extends Controller
{
    protected $providerObject;
    protected $userObject;

    public function __construct(ServiceProviderRepositoryInterface $providerRepository,AuthRepositoryInterface $userRepository)
    {
        $this->providerObject = $providerRepository;
        $this->userObject = $userRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function serviceProviderRequest(Request $request)
    {
        $lang = getallheaders()['Lang'];
        $Jwt = getallheaders()['Jwt'];
        $request['Jwt']=$Jwt;
        $result=$this->userObject->CheckJwt($Jwt);
        if($result=='false'){
            return response()->json(res_msg($lang, expired(), 403, 'Jwt_expired'));
        }
        $this->providerObject->serviceProviderRequest($request);
        return response()->json(res_msg($lang, success(), 200, 'request_sent'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getAllPlacesCategories()
    {
        $lang=getallheaders()['Lang'];
        $result= $this->providerObject->getAllPlacesCategories($lang);
        return response(res_msg($lang, success(),200,'all_categories', $result));
    }

    public function getAllServicesCategories()
    {
        $lang=getallheaders()['Lang'];
        $result= $this->providerObject->getAllServicesCategories($lang);
        return response(res_msg($lang, success(),200,'all_categories', $result));
    }

    public function getAllServicesSubCategories($category_id){

        $lang=getallheaders()['Lang'];
        $result= $this->providerObject->getAllServicesSubCategories($lang,$category_id);
        return response(res_msg($lang, success(),200,'all_subcategories', $result));

    }

    public function getAllServicesSubSubCategories($subcategory_id){

        $lang=getallheaders()['Lang'];
        $result= $this->providerObject->getAllServicesSubSubCategories($lang,$subcategory_id);
        return response(res_msg($lang, success(),200,'all_subsubcategories', $result));

    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getAllProviderPlacesCategoriesAndServices($lang)
    {
        $lang = getallheaders()['Lang'];
        $Jwt = getallheaders()['Jwt'];
        $result=$this->userObject->CheckJwt($Jwt);
        if($result=='false'){
            return response()->json(res_msg($lang, expired(), 403, 'Jwt_expired'));
        }
        $result = $this->providerObject->getAllProviderPlacesCategories($lang);
        $data['places_categories']=$result;

        $result = $this->providerObject->getAllProviderServices($lang);
        $data['servies']=$result;
        return response(res_msg($lang, success(), 200, 'all_provider_places_categories', $data));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function getAllPlacesCategoriesItems($place_category_id)
    {
        $lang = getallheaders()['Lang'];
        $result = $this->providerObject->getAllPlacesCategoriesItems($lang, $place_category_id);
        return response(res_msg($lang, success(), 200, 'all_places_categories_items', $result));

    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getPlaceDetails($place_id)
    {
        $lang = getallheaders()['Lang'];

        $Jwt = getallheaders()['Jwt'];
        $result = $this->userObject->CheckJwt($Jwt);
        if ($result == 'false') {
            return response()->json(res_msg($lang, expired(), 403, 'Jwt_expired'));
        }
        $result = $this->providerObject->getPlaceDetails($place_id,$lang);
        return response()->json(res_msg($lang, success(), 200, 'place_details', $result));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editPlace(Request $request)
    {
        $Jwt = getallheaders()['Jwt'];
        $lang = getallheaders()['Lang'];
        $result = $this->userObject->CheckJwt($Jwt);

        if ($result == 'false') {
            return response()->json(res_msg($lang, expired(), 403, 'Jwt_expired'));
        }
        $provider=Providers::where('user_id',$result->id)->pluck('id')->first();
        $request['provider_id']=$provider;
        $this->providerObject->editPlace($request);
        return response(res_msg($lang, success(), 200, 'done'));


    }
}
