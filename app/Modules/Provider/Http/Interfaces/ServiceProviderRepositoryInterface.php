<?php
namespace App\Modules\Provider\Http\Interfaces;

interface ServiceProviderRepositoryInterface
{
    public function serviceProviderRequest($request);
    public function getAllPlacesCategories($lang);
    public function getAllServicesCategories($lang);
    public function getAllServicesSubCategories($lang,$category_id);
    public function getAllServicesSubSubCategories($lang,$category_id);
    public function getAllProviderPlacesCategories($lang);
    public function getAllProviderServices($lang);
    public function getAllPlacesCategoriesItems($lang,$place_category_id);
    public function getPlaceDetails($place_id,$lang);
    public function editPlace($request);
}
