<?php

namespace App\Modules\Place\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('place', 'Resources/Lang', 'app'), 'place');
        $this->loadViewsFrom(module_path('place', 'Resources/Views', 'app'), 'place');
        $this->loadMigrationsFrom(module_path('place', 'Database/Migrations', 'app'), 'place');
        $this->loadConfigsFrom(module_path('place', 'Config', 'app'));
        $this->loadFactoriesFrom(module_path('place', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
