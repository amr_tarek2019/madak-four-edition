<?php

namespace App\Modules\Place\Http\Controllers\Api;

use App\Models\Providers;
use App\Modules\Auth\Http\Interfaces\AuthRepositoryInterface;
use App\Modules\Place\Http\Interfaces\PlaceRepositoryInterface;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class PlaceController extends Controller
{
    protected $placeObject;
    protected $userObject;

    public function __construct(PlaceRepositoryInterface $placeObject, AuthRepositoryInterface $userRepository)
    {
        $this->placeObject = $placeObject;
        $this->userObject = $userRepository;

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addPlace(Request $request)
    {
        $Jwt = getallheaders()['Jwt'];
        $lang = getallheaders()['Lang'];


            $validator = Validator::make($request->all(),
                array(
                    'place_category_id' => 'required',
                    'name' => 'required',
                    'present_type' => 'required',
                    'price' => 'required',
                    'quantity' => 'required',
                    'date_from' => 'required',
                    'date_to' => 'required',
                    'month' => 'required',
                    'year' => 'required',
                    'lat' => 'required',
                    'lng' => 'required',
                    'address' => 'required',
                    'desc' => 'required',
                    'main_image'=>'required'
                )
            );

        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'msg' => $validator->messages()]);
        }
        $result = $this->userObject->CheckJwt($Jwt);
        if ($result == 'false') {
            return response()->json(res_msg($lang, expired(), 403, 'Jwt_expired'));
        }
        $provider=Providers::where('user_id',$result->id)->pluck('id')->first();
        $request['provider_id'] = $provider;
        $this->placeObject->addPlace($request);
        return response(res_msg($lang, success(), 200, 'place_added'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
