<?php

namespace App\Modules\Notification\Http\Controllers\Api;

use App\Modules\Auth\Http\Interfaces\AuthRepositoryInterface;
use App\Modules\Notification\Http\Interfaces\NotificationRepositoryInterface;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class NotificationController extends Controller
{
    protected $notificationObject;
    protected $userObject;

    public function __construct(NotificationRepositoryInterface $notificationRepository,AuthRepositoryInterface $userRepository)
    {
        $this->notificationObject = $notificationRepository;
        $this->userObject = $userRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function GetAllNotifications(){
        $lang=getallheaders()['Lang'];
        $Jwt = getallheaders()['Jwt'];

        $result=$this->userObject->CheckJwt($Jwt);
        if($result=='false'){
            return response()->json(res_msg($lang, expired(), 403, 'Jwt_expired'));
        }

        $result= $this->notificationObject->GetNotifications($lang,$result->id);
        return response(res_msg($lang, success(),200,'all_notifications', $result));



    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
