<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesSubSubCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services_sub_sub_categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('service_subcategory_id')->unsigned();
            $table->string('image');
            $table->string('name');
            $table->boolean('status')->default(0);
            $table->timestamps();
            $table->foreign('service_subcategory_id')
                ->references('id')->on('services_subcategories')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services_sub_sub_categories');
    }
}
