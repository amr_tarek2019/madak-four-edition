<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('provider_id')->unsigned();
            $table->bigInteger('service_category_id')->unsigned();
            $table->bigInteger('service_subcategory_id')->unsigned();
            $table->bigInteger('service_sub_sub_category_id')->unsigned();
            $table->string('present_type');
            $table->string('price');
            $table->string('quantity');
            $table->string('lat');
            $table->string('lng');
            $table->string('address');
            $table->string('date_from');
            $table->string('date_to');
            $table->string('month');
            $table->string('year');
            $table->string('image');
            $table->text('desc');
            $table->timestamps();
            $table->foreign('provider_id')
                ->references('id')->on('providers')
                ->onDelete('cascade');
            $table->foreign('service_category_id')
                ->references('id')->on('services_categories')
                ->onDelete('cascade');
            $table->foreign('service_subcategory_id')
                ->references('id')->on('services_subcategories')
                ->onDelete('cascade');
            $table->foreign('service_sub_sub_category_id')
                ->references('id')->on('services_sub_sub_categories')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
