<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('places', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('provider_id')->unsigned();
            $table->bigInteger('place_category_id')->unsigned();
            $table->string('name');
            $table->string('present_type');
            $table->string('price');
            $table->string('quantity');
            $table->string('date_from');
            $table->string('date_to');
            $table->string('month');
            $table->string('year');
            $table->string('lat');
            $table->string('lng');
            $table->string('address');
            $table->text('desc');
            $table->string('main_image');
            $table->timestamps();
            $table->foreign('provider_id')
                ->references('id')->on('providers')
                ->onDelete('cascade');

            $table->foreign('place_category_id')
                ->references('id')->on('places_categories')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('places');
    }
}
