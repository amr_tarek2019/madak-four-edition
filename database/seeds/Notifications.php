<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class Notifications extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for($i = 0 ;$i< 10 ;$i++){
            $array = [
                'user_id' => 1,
                'icon'=>'default.png',
                'title'=>$faker->name,
                'text'=>$faker->text,
            ];
            \App\Models\Notification::create($array);
        }
    }
}
