<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
class ServicesCategories extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        for($i = 0 ;$i< 10 ;$i++){
            $array = [
                'image'=>'default.png',
                'name' => $faker->word,
            ];
            \App\Models\ServiceCategory::create($array);
        }
    }
}
